provider "google" {
  credentials = file("./dufuna-devops-f9f85ab07232.json")
  project = "dufuna-devops"
  region  = "us-central1"
  zone    = "us-central1-a"
}

resource "google_compute_instance" "dufuna-instance" {
  name         = "dufuna-instance"
  machine_type = "e2-micro"
  zone = "us-central1-a"
  project = "dufuna-devops"
  tags         = ["http-server"]
   boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }
   network_interface {
    network = "default"
    access_config {
    }
  }
}